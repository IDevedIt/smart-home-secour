export class DomoticItem {
  id: number;
  Name: string;

  constructor(id: number, name: string) {
    this.id = id;
    this.Name = name;
  }
}
